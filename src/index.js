const {GraphQLServer} = require('graphql-yoga');
const {PrismaClient} = require('@prisma/client');
const {PubSub} = require('graphql-yoga');

const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutations');
const Subscription = require('./resolvers/Subscription');
const Link = require('./resolvers/Link');
const User = require('./resolvers/User');
const Vote = require('./resolvers/Vote');

const resolvers = {
    Query,
    Mutation,
    Subscription,

    Link,
    User,
    Vote,
};

const prisma = new PrismaClient();
const pubsub = new PubSub();

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context: request => {
        return {
            ...request,
            prisma,
            pubsub
        }
    },
});

server.start(async () => await console.log(`Server is running on http://localhost:4000`));
