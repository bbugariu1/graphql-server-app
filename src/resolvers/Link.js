function postedBy(parent, args, context) {
    return context.prisma.link.findOne({ where: { id: parent.id } }).postedBy();
}

function votes(parent, args, context) {
    return context.prisma.link.findOne({ where: { id: parent.id } }).votes();
}

module.exports = {
    id: parent => parent.id,
    url: parent => parent.url,
    description: parent => parent.description,
    postedBy,
    votes,
};