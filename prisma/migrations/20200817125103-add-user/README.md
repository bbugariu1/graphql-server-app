# Migration `20200817125103-add-user`

This migration has been generated by Bugariu Bogdan at 8/17/2020, 3:51:03 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
PRAGMA foreign_keys=OFF;

CREATE TABLE "User" (
"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
"name" TEXT NOT NULL,
"email" TEXT NOT NULL)

PRAGMA foreign_key_check;

PRAGMA foreign_keys=ON;
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200816165741-init..20200817125103-add-user
--- datamodel.dml
+++ datamodel.dml
@@ -1,8 +1,8 @@
 // 1
 datasource db {
   provider = "sqlite" 
-  url = "***"
+  url = "***"
 }
 // 2
 generator client {
@@ -16,8 +16,9 @@
   description String
   url         String
 }
-// model User {
-//     id      Int @id @default(autoincrement())
-//     name    String
-// }
+model User {
+  id          Int @id @default(autoincrement())
+  name        String
+  email       String
+}
```


